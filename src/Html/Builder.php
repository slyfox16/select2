<?php

namespace Alexwijn\Select2\Html;

use App\Facades\Asset;

/**
 * Alexwijn\Select2\Html\Builder
 */
class Builder
{
    /** @var \Alexwijn\Select2\Dropdown */
    protected $dropdown;

    /**
     * @var array
     */
    protected $parameters;

    /**
     * Builder constructor.
     *
     * @param \Alexwijn\Select2\Dropdown $dropdown
     */
    public function __construct(\Alexwijn\Select2\Dropdown $dropdown)
    {
        $this->dropdown = $dropdown;
    }

    /**
     * Customize the select2 options.
     *
     * @param array $parameters
     * @return \Alexwijn\Select2\Html\Builder
     */
    public function parameters(array $parameters): Builder
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * Render the select2 javascript element.
     *
     * @param string $element
     * @param $parameters
     * @return string
     */
    public function render(string $element, $parameters)
    {
        $html = '';

        try {
            $parameters = collect($this->parameters)->merge($parameters);
            Asset::setParams($parameters);
            $parameters = Asset::getParams();
            Asset::script("$('{$element}').select2({$parameters});");
        } catch (\Throwable $e) {
            return '';
        }
    }

    /**
     * @return string
     */
    private function addScripts()
    {
        $scripts = '';
        if (config('fileinput.jquery') !== NULL) {
            $scripts .= '<script src="//code.jquery.com/jquery-3.2.1.min.js"></script>';
        }

        $scripts .= '<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>';

        return $scripts;
    }

    /**
     * @return string
     */
    private function addStyles()
    {
        $styles = '';
        $styles .= '<link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" media="all" rel="stylesheet" type="text/css" />';

        return $styles;
    }
}
